import React, {Component} from 'react';
import {
    View,
    Text,
    Button,
    AppRegistry,
    ScrollView,
    FlatList,
    StyleSheet
} from 'react-native';
import CalendarEntry from './CalendarEntry';

import AuthService from '../../services/AuthService';
import CalendarService from '../../services/CalendarService';

const authService = new AuthService();

export default class Calendar extends Component {
    static navigationOptions = {
        title: 'Calendar' //header
    }

    constructor(props) {
        super(props);
        this.state = {
            calendarService: new CalendarService(),
            entries: [],
            userId: null,
            hasCourses: false
        }

        this.getEntries();
    }

    logout() {
        authService.logout();
        this.props.navigation.navigate('Home');
    }

    async getEntries() {
        console.log('getting entries');

        let courses = await this.state.calendarService.getAllCoursesAfterDate();
        console.log('Caledar - printing courses:');
        console.log(courses);

        this.setState({
            entries: courses,
            hasCourses: true
        });
    }

    testEntries() {
        console.log('testing entries');
        console.log(this.state.entries);
    }

    renderList() {
        if (this.state.hasCourses) {
            return (
                <FlatList
                    data={this.state.entries}
                    renderItem={({item}) => (
                        // <Text>{item.title}</Text>
                        <CalendarEntry
                            title={item.title}
                            startTime={item.startTime}
                            endTime={item.endTime}
                            year={item.year}
                            month={item.month}
                            day={item.day}
                            dayName={item.dateName}
                        />
                    )}
                    keyExtractor={(entry, index) => index}
                />);
        }
    }

    render() {
        return (
            <View
                style={styles.mainContainer}
            >
                <ScrollView
                    style={styles.mainContainer}
                >
                    {this.renderList()}

                </ScrollView>

                <View
                    style={styles.buttonWrapper}
                >
                    <Button
                        title='logout'
                        color='#f66'
                        onPress={() => this.logout()}
                    />
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },

    buttonWrapper: {
        padding: 5
    }
});
AppRegistry.registerComponent('Calendar', () => Calendar)  