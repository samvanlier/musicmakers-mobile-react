import React, {Component} from 'react';
import {
    View,
    Text,
    AppRegistry,
    StyleSheet
} from 'react-native';

export default class CalenderEntry extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View
            style={styles.mainContainer}
            >
                <View
                    style={styles.dates}
                >
                    <Text
                    style={styles.dateName}
                    >{this.props.dayName}</Text>
                    <Text
                    style={styles.date}
                    >{this.renderDate()}</Text>
                </View>
                <View
                style={styles.line}
                />

                <View
                style={styles.entryRow}
                >
                    <View
                    style={styles.entryCol}
                    >
                        <Text>{this.props.startTime}</Text>
                        <Text>{this.props.endTime}</Text>
                    </View>
                    <View
                    style={styles.titleView}
                    >
                        <Text
                        style={styles.title}
                        >{this.props.title}</Text>
                    </View>
                </View>

                {/* <Text>{this.props.dayName}</Text> */}
                {/* <Text>{this.props.title}</Text>
                <Text>{this.props.startTime}</Text>
                <Text>{this.props.endTime}</Text> */}
                {/* <Text>{this.props.year}</Text>
                <Text>{this.props.month}</Text>
                <Text>{this.props.day}</Text> */}
            </View>
        );
    }

    renderDate() {
        return this.props.day + "/"+ this.props.month + "/"+this.props.year;
    }
}

const styles = StyleSheet.create({
    mainContainer:{
        flex: 1,
        padding: 5
    },
    dates:{
        flex:1,
        flexDirection: 'row'
    },
    dateName:{
        flex: 3,
        fontWeight: 'bold',
        fontSize: 16
    },
    date:{
        flex:1,
        // alignSelf: 'flex-end'
        textAlign: 'left'
    },
    line:{
        borderBottomColor: 'black',
        borderBottomWidth: 1,
    },
    entryRow: {
        flex: 1,
        flexDirection: 'row'
    },
    entryCol :{
        flex: 1,
        flexDirection: 'column'
    },
    titleView: {
        flex: 2
    },
    title :{
        fontWeight: 'bold',
        fontSize: 16
    }
});

AppRegistry.registerComponent('CalenderEntry', () => CalenderEntry);