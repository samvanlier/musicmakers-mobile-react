import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    Button,
    View,
    AppRegistry,
    TouchableHighlight,
    TextInput,
    AsyncStorage
} from 'react-native';

import CustomDatePicker from '../FormComponents/CustomDatePicker';

export default class Test extends Component {
    static navigationOptions = {
        title: 'Musicmakers', //header
      };

    constructor(props) {
        super(props);
        this.state = {
            name: 'Sam',
            message: this.props.message,
            date: '',
            email: ''
        }
       this.checkLogin();
    }

    async checkLogin() {
        var isAuthenticated = await AsyncStorage.getItem('@Athenticated:key');
        console.log('test: isAuthenticated='+isAuthenticated);
        
        if (isAuthenticated !== null){
            if(isAuthenticated){
                console.log('al ingelogd!');
                this.props.navigation.navigate('Calendar');
            }
        } else {
            console.log('not authenticated');
        }
    }

    onChangeEmail(value) {
        this.setState({
            email: value
        });
    }

    render() {
        return (
            <View>
                <Text>
                    {this.state.date}
                </Text>

                <Button
                title="Go to Login"
                onPress={() => this.props.navigation.navigate('Login')}
                />

                <Button
                title="Go to Signup"
                onPress={() => this.props.navigation.navigate('Signup')}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    message: {
        color: 'red'
    },
    container: {
        flexDirection: 'row',
        height: 100
    },
    v1: {
        flex: 1,
        backgroundColor: 'red',
        padding: 10
    },
    v2: {
        flex: 1,
        backgroundColor: 'green',
        padding: 10
    },
    v3: {
        flex: 1,
        backgroundColor: 'pink',
        padding: 10
    }
});

AppRegistry.registerComponent('test', () => Test)
  