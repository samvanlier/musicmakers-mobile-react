import React, {Component} from 'react';
import {
    Text,
    Button,
    ScrollView,
    AppRegistry
} from 'react-native';
import StorageService from '../../services/StorageService';

export default class TestStorage extends Component {
    static navigationOptions = {
        title: 'TestStorage', //header
    };

    constructor(props){
        super(props);

        this.state = {
              storageService : new StorageService,
              idToken: null
        };
    }

    render(){
        return (
            <ScrollView>
                <Button
                title="test id token"
                onPress={() => this.testIdToken()}
                />
                <Text>
                    {this.state.idToken}
                </Text>
            </ScrollView>

        );
    }

   async testIdToken(){
        console.log('start test: id token');

        this.setState({
            idToken: await this.state.storageService.getIdToken()
        });

        console.log('end test: id token')
    }
}