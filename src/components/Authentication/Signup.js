import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    AppRegistry,
    TextInput,
    Button,
    ScrollView
} from 'react-native';
import CustomDatePicker from '../FormComponents/CustomDatePicker';

import AuthService from '../../services/AuthService';

const authService = new AuthService();

export default class Signup extends Component {
    static navigationOptions = {
        title: 'Signup', //header
      };
    constructor() {
        super();
        this.focusNextField = this.focusNextField.bind(this);
        this.inputs = {};
        this.state = {
            email: '', //credentials
            password: '', //credentials
            firstName: '', //account
            lastName: '', //account
            street: '', //account.address
            number: '', //account.address
            zip: '', //account.address
            city: '', //account.address
            country: '', //account.address
            birthdate: '', //account

            confirmPassword: null,
            match: false,
            error: '',
            users: []
        }
    }

    focusNextField(id) {
        this.inputs[id].focus();
    }

    onChangeEmail(email) {
        //todo
        this.setState({
            email: email
        });
    }

    onChangePassword(password) {
        this.setState({
            password: password
        });
    }

    onChangePasswordCheck(passwordToCheck) {
        // console.log('password to check: ' + passwordToCheck);
        // console.log('password: ' + this.state.password);
        this.setState({
            passwordToCheck: passwordToCheck
        });
    }

    comparePasswords(){
        if (this.passwordToCheck === this.password) {
            this.setState({
                match: true
            });
            console.log("wachtwoorden komen overeen");
        } else {
            this.setState({
                match: false
            });
            //todo: error messages?
            console.log("wachtwoorden komen niet overeen");
        }
    }

    onChangeFirstName(firstName) {
        this.setState({
            firstName: firstName
        });
    }

    onChangeLastName(lastName) {
        this.setState({
            lastName: lastName
        });
    }

    onChangeStreet(street) {
        this.setState({
            street: street
        });
    }

    onChangeStreetNumber(number) {
        this.setState({
            number: number
        });
    }

    onChangeCity(city) {
        this.setState({
            city: city
        });
    }

    onChangezip(zip) {
        this.setState({
            zip: zip
        });
    }

    onChangeCountry(country) {
        this.setState({
            country: country
        });
    }

    onChangeBirthday = (value) => {
        this.setState({
            birthdate: value
        });
    }

    async submit() {

        if (this.state.match) {
            let credentials = {
                username: this.state.email,
                password: this.state.password
            };

            let account = {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                address: {
                    street: this.state.street,
                    number: this.state.number,
                    zip: this.state.zip,
                    city: this.state.city,
                    country: this.state.country
                },
                birthdate: this.state.birthdate,
                email: this.state.email
            }

            let succes = await authService.signup(credentials, account);
            if (succes) {
                 this.props.navigation.navigate('Calendar');
            }
           
        } else {
            //  this.onChangeError('de wachtwoorden koment niet overeen');
            console.log('passwords not correct');
        }
    }

    render() {
        return (
            <ScrollView>
                <TextInput
                    onSubmitEditing={() => {
                        this.focusNextField('Password');
                    }}
                    ref={ input => {
                        this.inputs['Email'] = input;
                    }}
                    blurOnSubmit={ false }
                    placeholder='Email'
                    value={this.state.email}
                    onChangeText={(value) => this.onChangeEmail(value)}
                    autoCapitalize="none"
                    returnKeyType="next"
                    keyboardType='email-address'
                />
                <TextInput
                    blurOnSubmit={ false }
                    ref={ input => {
                        this.inputs['Password'] = input;
                    }}
                    onSubmitEditing={() => {
                        this.focusNextField('NextPassword');
                    }}
                    placeholder='Wachtwoord'
                    value={this.state.password}
                    onChangeText={(value) => this.onChangePassword(value)}
                    returnKeyType="next"
                    secureTextEntry
                />
                <TextInput
                    blurOnSubmit={ false }
                    ref={ input => {
                        this.inputs['NextPassword'] = input;
                    }}
                    onSubmitEditing={() => {
                        this.focusNextField('FirstName');
                    }}
                    placeholder='Bevestig Wachtwoord'
                    value={this.state.confirmPassword}
                    onChangeText={(value) => this.onChangePasswordCheck(value)}
                    onBlur={() => this.comparePasswords()}
                    returnKeyType="next"
                    secureTextEntry
                />
                <TextInput
                    blurOnSubmit={ false }
                    ref={ input => {
                        this.inputs['FirstName'] = input;
                    }}
                    onSubmitEditing={() => {
                        this.focusNextField('LastName');
                    }}
                    placeholder='Voornaam'
                    value={this.state.firstName}
                    onChangeText={(value) => this.onChangeFirstName(value)}
                    autoCapitalize="words"
                    returnKeyType="next"
                />
                <TextInput
                    blurOnSubmit={ false }
                    ref={ input => {
                        this.inputs['LastName'] = input;
                    }}
                    onSubmitEditing={() => {
                        this.focusNextField('street');
                    }}
                    placeholder='Achternaam'
                    value={this.state.lastName}
                    onChangeText={(value) => this.onChangeLastName(value)}
                    autoCapitalize="words"
                    returnKeyType="next"
                />
                <TextInput
                    blurOnSubmit={ false }
                    ref={ input => {
                        this.inputs['street'] = input;
                    }}
                    onSubmitEditing={() => {
                        this.focusNextField('number');
                    }}
                    placeholder='Straat'
                    value={this.state.street}
                    onChangeText={(value) => this.onChangeStreet(value)}
                    autoCapitalize="words"
                    returnKeyType="next"
                />
                <TextInput
                    blurOnSubmit={ false }
                    ref={ input => {
                        this.inputs['number'] = input;
                    }}
                    onSubmitEditing={() => {
                        this.focusNextField('city');
                    }}
                    placeholder='Nr'
                    value={this.state.number}
                    onChangeText={(value) => this.onChangeStreetNumber(value)}
                    keyboardType="numeric"
                    returnKeyType="next"
                />
                <TextInput
                    blurOnSubmit={ false }
                    ref={ input => {
                        this.inputs['city'] = input;
                    }}
                    onSubmitEditing={() => {
                        this.focusNextField('zip');
                    }}
                    placeholder='Gemeente'
                    value={this.state.city}
                    onChangeText={(value) => this.onChangeCity(value)}
                    autoCapitalize="words"
                    returnKeyType="next"
                />
                <TextInput
                    blurOnSubmit={ false }
                    ref={ input => {
                        this.inputs['zip'] = input;
                    }}
                    onSubmitEditing={() => {
                        this.focusNextField('Country');
                    }}
                    placeholder='Postcode'
                    value={this.state.zip}
                    onChangeText={(value) => this.onChangezip(value)}
                    keyboardType="numeric"
                    returnKeyType="next"
                />
                <TextInput
                    blurOnSubmit={ false }
                    ref={ input => {
                        this.inputs['Country'] = input;
                    }}
                    placeholder='Land'
                    value={this.state.country}
                    onChangeText={(value) => this.onChangeCountry(value)}
                    autoCapitalize="words"
                    returnKeyType="next"
                />
                <CustomDatePicker
                    onBirthdayChanged={this.onChangeBirthday}
                />
                <Button
                    onPress={() => {
                        this.submit(this.state.email, this.state.password, this.state.match,
                            this.state.firstName, this.state.lastName, this.state.street, this.state.number,
                            this.state.city, this.state.zip, this.state.country, this.state.birthdate);
                    }}
                    title='Singup'
                    color='#f66'
                    accessibilityLabel="press to signup" //voor blinde mensen blijkbaar.. who knew
                />
            </ScrollView>
        );
    }
}

AppRegistry.registerComponent('Signup', () => Signup)