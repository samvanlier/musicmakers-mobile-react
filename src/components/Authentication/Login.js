import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    AppRegistry,
    TextInput,
    Button,
} from 'react-native';
import AuthService from '../../services/AuthService';
const authService = new AuthService();

export default class Login extends Component {

    static navigationOptions = {
        title: 'Login', //header
      };

    constructor(props) {
        super(props);
        this.focusNextField = this.focusNextField.bind(this);
        this.inputs = {};
        this.state = {
            email: '',
            password: ''
        }
    }

    focusNextField(id) {
        this.inputs[id].focus();
    }

    onChangeEmail(value) {
        this.setState({
            email: value
        });
    }

    onChangePassword(password) {
        this.setState({
            password: password
        });
    }

    async submit(email, password) {
        let credentials = {
            username: email,
            password: password
        };

        authService.addHeaders();
        let succesLogin = await authService.login(credentials);
        if (succesLogin){
            console.log("login succesfull -> do routing");
            this.props.navigation.navigate('Calendar');
        } else {
            console.log("login falled...");
        }
    }

    render() {
        return (
            <View>
                <TextInput
                    onSubmitEditing={() => {
                        this.focusNextField('Password');
                    }}
                    ref={ input => {
                        this.inputs['email'] = input;
                    }}
                    blurOnSubmit={ false }
                    className="email"
                    placeholder='Email'
                    value={this.state.email}
                    onChangeText={(value) => this.onChangeEmail(value)}
                    autoCapitalize="none"
                    returnKeyType="next"
                    keyboardType='email-address'
                />
                <TextInput
                    blurOnSubmit={ false }
                    ref={ input => {
                        this.inputs['Password'] = input;
                    }}
                    placeholder='Wachtwoord'
                    value={this.state.password}
                    onChangeText={(value) => this.onChangePassword(value)}
                    returnKeyType="go"
                    secureTextEntry
                />
                <Button
                    onPress={() => this.submit(this.state.email, this.state.password)}
                    title='Login'
                    color='#f66'
                    accessibilityLabel="press to login" //voor blinde mensen blijkbaar.. who knew
                />
            </View>
        );
    }
}

AppRegistry.registerComponent('Login', () => Login)  