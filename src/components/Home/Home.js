import React, {Component} from 'react';
import {
    View,
    Text,
    Button,
    AppRegistry,
    StyleSheet
} from 'react-native';
import StorageService from '../../services/StorageService';

export default class Home extends Component {
    static navigationOptions = {
        title: 'Music Makers', //header
        display:false
    };

    constructor(props){
        super(props);

        this.state = {
            storageService : new StorageService
        };

        this.checkLogin();
    }

    async checkLogin() {
        var isAuthenticated = await this.state.storageService.getAuthentication();
        console.log('is authenticated? ' + isAuthenticated);

        if (isAuthenticated !== null){
            if (isAuthenticated == 'true') {
                console.log('al ingelogt -> go to calendar');
                this.props.navigation.navigate('Calendar');
            }
        }
    }

    render(){
        return (
            <View
            style= {styles.mainContainer}
            >
                <View 
                style={styles.container}
                >
                    <Text style={styles.title}>Music ♪ Makers</Text>
                </View>
                <View
                style={styles.containerButton}
                >
                    <View
                    style={styles.buttonWrapper}>
                        <Button
                        style={styles.button}
                        title="Go to Login"
                        color='#f66'
                        onPress={() => this.props.navigation.navigate('Login')}
                        />
                    </View>
                    <View 
                    style={styles.buttonWrapper}>        
                        <Button
                        style={styles.button}
                        title="Go to Signup"
                        color='#f66'
                        onPress={() => this.props.navigation.navigate('Signup')}
                        />
                    </View>    
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer:{
        flex: 1
    },

    container:{
        flex:2,
        flexDirection:'column',
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: 'red'
    },

    title: {
        color: '#f66',
        fontSize: 40,
        fontWeight: 'bold'
    },

    containerButton :{
        flex: 1,
        // justifyContent: 'center',
        margin: 10
    },

    buttonWrapper: {
        padding: 5
    },

    button :{
        // marginTop: 10
        // color: '#f66'
    }
});

AppRegistry.registerComponent('Home', () => Home);