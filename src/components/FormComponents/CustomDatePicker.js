import React, { Component } from 'react';
import {
    AppRegistry
  } from 'react-native';

import PropTypes from 'prop-types';
import DatePicker from 'react-native-datepicker';

export default class CustomDatePicker extends Component {
    constructor(props){
        super(props)
        this.state = {
          currentDate: new Date(),
          date: this.currentDate
        }
      }

      onDateChange(value){
        this.setState({
            date: value,
            currentDate: value
        });
        this.props.onBirthdayChanged(value)
      }
     
      render(){
        return (
          <DatePicker
            date={this.state.currentDate}
            mode="date"
            placeholder="select date"
            format="DD-MM-YYYY"
            minDate="01-01-1900"
            maxDate= {new Date()}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                position: 'absolute',
                left: 0,
                top: 4,
                marginLeft: 0
              },
              dateInput: {
                marginLeft: 36
              }
            }}
            onDateChange={(value) => this.onDateChange(value)}
          />
        )
      }
}
AppRegistry.registerComponent('CustomDatePicker', () => CustomDatePicker);