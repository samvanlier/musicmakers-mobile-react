import {AsyncStorage} from 'react-native';
import axios from 'axios';
import StorageService from './StorageService';

// const storageService;
export default class AuthServer {
  constructor() {
    this.storageService =  new StorageService();
    this.apiurl = 'https://superteam-testgateway.herokuapp.com/api/auth/';
    this.loginurl = this.apiurl + 'userauth/oauth/token';
    this.signupurl = this.apiurl + 'userauth/signup';

    this.user = {
      authenticated: false
    }

    this.succesLogin = false;
  }

  async getEmail() {
    console.log('getting mail');
    return await axios('https://superteam-testgateway.herokuapp.com/api/auth/userauth/me', {
      method: "get",
      headers: await this.getAuthHeader(),
    });
  };

  addHeaders(){
    //axios.defaults.headers.common['Access-Control-Allow-Credentials:'] = '';
    //axios.defaults.headers.common['Access-Control-Allow-Origin'] = 'https://superteam-testgateway.herokuapp.com/';
    axios.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded';
    axios.defaults.headers.common['Accept'] = 'application/json';
  };

  async login(creds) {
    this.user.authenticated = false;
    let url = this.loginurl + '?grant_type=password&username='+creds.username+'&password='+creds.password;
    console.log(creds);
    
    await axios(url, {
      method: "post",
      data: creds,
      withCredentials: false,
      auth: {
        username: 'acme',
        password: 'acmesecret'
      }
    })
      .then(response => {
        
        try {
          // AsyncStorage.setItem('@id_token:key', response.data.jti);
          this.storageService.setIdToken(response.data.jti);
          // AsyncStorage.setItem('@access_token:key', response.data.access_token);
          this.storageService.setAccessToken(response.data.access_token);
        } catch (error){
          console.log(error);
        }
        
        this.setEmail();
        
        // AsyncStorage.setItem('@Autheticated:key', 'true');
        this.storageService.setAuthentication(true);  

        this.user.authenticated = true;
        console.log("auth? "+ this.user.authenticated)
      })
      .catch(function (error) {
        console.log(error);
      });

      return this.user.authenticated;
  }

  async signup(creds, account) {
    let url = 'https://superteam-testgateway.herokuapp.com/api/auth/userauth/signup/';

    await axios(url, {
      method: "post",
      data: creds,
      withCredentials: false,
      auth: {
        username: 'acme',
        password: 'acmesecret'
      },
      headers: {
        "Content-Type": "application/json"
      }
    }).then(response => {
      if(response.status === 200){
        this.createAccount(account);
        return this.login(creds);
      }
    })
      .catch((error) => {
        console.log(error);
      });

    return this.user.authenticated;
  }

  getUser(){
    return this.user;
  }

  async createAccount(account){
    axios('https://superteam-testgateway.herokuapp.com/api/account/create', {
      method: "post",
      data: account,
      headers: await this.getAuthHeader()
    }).then(response => {
      console.log("response of createAccount:");
      console.log(response);
    })
      .catch((error) => {
        console.log(error);
      });
  }

  logout() {
    console.log("logout called");
    // AsyncStorage.removeItem('@id_token:key');
    this.storageService.removeIdToken();
    // AsyncStorage.removeItem('@access_token:key');
    this.storageService.removeAccesToken()
    // AsyncStorage.removeItem('@user_id:key');

    // AsyncStorage.setItem('@Autheticated:key', 'false');
    this.storageService.setAuthentication(false);
    this.user.authenticated = false;
  }

  async checkAuth() {
    // let jwt = await AsyncStorage.getItem('@id_token:key');
    let jwt = await this.storageService.getIdToken();
    if(jwt) {
      this.user.authenticated = true
    }
    else {
      this.user.authenticated = false
    }
  }

  // The object to be passed as a header for authenticated requests
   async getAuthHeader() {
     let access_token = await this.storageService.getAccessToken()
    return {
      // 'Authorization': 'Bearer ' + await AsyncStorage.getItem('@access_token')
      'Authorization': 'Bearer ' + access_token
    }
  }

  async getAccount(username){
    console.log("[UserService] Username in getAccount:"+username);
    return await axios('https://superteam-testgateway.herokuapp.com/api/account/email/' + username, {
      method: "get",
      //data: username,
      // headers: this.auth.getAuthHeader()
      headers: this.getAuthHeader()
    })
  }

  async setEmail() {
    console.log('autservice: setting email!');
    let username = await this.getEmail();
    console.log('username:' + username);
    let response = await this.getAccount(username.data.name);
    console.log('response: ' +response);
    
    let userId = response.data.userId;
    console.log('authservice: userid='+userId);

    // AsyncStorage.setItem('@user_id:key', userId);
    this.storageService.setUserId(userId);
  }
}