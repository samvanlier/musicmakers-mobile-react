import {AsyncStorage} from 'react-native';

export default class StorageService {
    constructor(){
        //empty
    }

    setIdToken(value) {
        console.log('StorageService: setting id token ' + value)
        try{
            AsyncStorage.setItem('@id_token:key', value);
        } catch (error) {
            this.logError(error);
        }
    }

    async getIdToken(){
        try{
            return await AsyncStorage.getItem('@id_token:key');
        } catch(error){
            this.logError(error);
        }
    }
    
    removeIdToken(){
        AsyncStorage.removeItem('@id_token:key');
        console.log('id token removed');
    }

    setAuthentication(value) {
        console.log('StorageService: setting authentication ' + value);
        try {
            AsyncStorage.setItem('@Autheticated:key', value + '');
        } catch (error) {
            this.logError(error);
        }
    }

    async getAuthentication(){
        try{
            return await AsyncStorage.getItem('@Autheticated:key');
        } catch(error){
            this.logError(error);
        }
    }

    setAccessToken(value) {
        console.log('StorageService: setting access token ' + value);
        try {
            AsyncStorage.setItem('@access_token:key', value);
        } catch (error) {
            this.logError(error);
        }
    }

    async getAccessToken(value) {
        try{
            return await AsyncStorage.getItem('@access_token:key');
        } catch(error){
            this.logError(error);
        }
    }

    removeAccesToken(){
        AsyncStorage.removeItem('@access_token:key');
        console.log('access token removed');
    }

    setUserId(value) {
        console.log('StorageService: setting user id ' + value);
        try {
            AsyncStorage.setItem('@user_id:key', value + '');
        } catch (error) {
            this.logError(error);
        }
    }

    async getUserId(){
        try{
            return await AsyncStorage.getItem('@user_id:key');
        } catch(error){
            this.logError(error);
        }
    }

    removeUserId(){
        AsyncStorage.removeItem('@user_id:key');
        console.log('user id removed');
    }

    /*
    * error logging
    */
    logError(error){
        console.log('StorageService: ' + error);
    }
}