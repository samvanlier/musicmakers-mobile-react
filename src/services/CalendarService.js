import axios from 'axios';

import AuthService from './AuthService';
import StorageService from './StorageService';

export default class CalendarService {
    constructor() {
        this.agendaUrl  = "https://superteam-testgateway.herokuapp.com/api/agenda/agendaservicemobile/";
        this.authService = new AuthService();
        this.storageService = new StorageService();
        this.courses = [];
        this.tempUserId = 15;

        // this.getAllCoursesAfterDate();
    }

    getCourses(){
        return this.courses;
    }

    async getAllCoursesAfterDate(){
        let userId = await this.storageService.getUserId();
        console.log("CalendarSerive: getting courses for user with id "+this.tempUserId);
        let authHeader = await this.authService.getAuthHeader();
        console.log(authHeader);
        await axios(
            this.agendaUrl+"getAllCoursesAfterDate/" + this.tempUserId,
            {
                method: "get",
                headers: authHeader
            }
        ).then((response) => {
            console.log('printing response.data:')
            console.log(response.data);
            this.courses = response.data
            console.log("response="+this.courses[0].title);
            // return response.data;
        }).catch((error) => {
            console.log('CalendarService: '+error);
        });
        console.log('retrieved courses: ');
        console.log(this.courses);
        return this.courses;
    }

}