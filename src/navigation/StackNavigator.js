import {StackNavigator} from 'react-navigation'; //src=https://reactnavigation.org/ (zie docs voor navigation)

import Home from '../components/Home/Home';
import Login from '../components/Authentication/Login';
import Signup from '../components/Authentication/Signup';
import Calendar from '../components/Calendar/Calendar';

import Test from '../components/Test/Test'; //TODO remove for deploy
import TestStorage from '../components/Test/TestStorage'; //TODO remove for deploy

export default StackNavigator(
    {
      Home: {
        screen: Home,
        navigationOptions: {
          header: null
        }
      },
      Login: {
        screen: Login
      },
      Calendar : {
        screen: Calendar
      },
      Signup: {
        screen: Signup
      },
      TestStorage: {
        screen: TestStorage
      }
    },
    {
      initialRouteName: 'Home'
    }
);