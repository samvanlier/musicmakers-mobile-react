import Login from "../src/components/Authentication/Login";
import * as React from "react";
import renderer from 'react-test-renderer';
import Calendar from "../src/components/Calendar/Calendar";
import CalenderEntry from "../src/components/Calendar/CalendarEntry";

test( 'Login view renders correctly', () => {
    const tree = renderer.create(
        <Login />
    ).toJSON()
} );

test( 'CalenderEntry view renders correctly', () => {
    const tree = renderer.create(
        <CalenderEntry />
    ).toJSON()
} );

test( 'Test Calendar Entry with info', () => {
    const tree = renderer.create(
        <CalenderEntry
            title="feest na goed IP2 examen"
            startTime="12:00"
            endTime="14:00"
            year="2018"
            month="03"
            day="23"
            dayName="friday"
        />
    ).toJSON()
} );