import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  AppRegistry
} from 'react-native';
// import {StackNavigator} from 'react-navigation'; //src=https://reactnavigation.org/ (zie docs voor navigation)
import RootStack from './src/navigation/StackNavigator';

import PropTypes from 'prop-types';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

// const RootStack = StackNavigator(
//   {
//     Home: {
//       screen: Test
//     },
//     Login: {
//       screen: Login
//     },
//   },
//   {
//     initialRouteName: 'Home'
//   }
// );


export default class App extends Component {
  render() {
      return <RootStack/>;
    
  }
}
AppRegistry.registerComponent('app', () => App)